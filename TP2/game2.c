#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    srand(time(NULL));  

    int nombreAleatoire = rand() % 100 + 1;

    int proposition, tentatives = 0;

    printf("Devinez le nombre entre 1 et 100 :\n");

    do {

        printf("Proposition : ");
        scanf("%d", &proposition);


        if (proposition == nombreAleatoire) {
            printf("Félicitations ! Vous avez trouvé le bon nombre en %d tentatives.\n", tentatives + 1);
        } else if (proposition < nombreAleatoire) {
            printf("Plus grand !\n");
        } else {
            printf("Plus petit !\n");
        }


        tentatives++;

    } while (proposition != nombreAleatoire);

    return 0;
}


