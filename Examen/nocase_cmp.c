#include <stdio.h>


int nocase_equal(char *s1, char *s2) {
    while (*s1 != '\0' && *s2 != '\0') {

        char c1 = (*s1 >= 'A' && *s1 <= 'Z') ? (*s1 + 32) : *s1;
        char c2 = (*s2 >= 'A' && *s2 <= 'Z') ? (*s2 + 32) : *s2;


        if (c1 != c2) {
            return 0; 
        }

        s1++;
        s2++;
    }


    return (*s1 == '\0' && *s2 == '\0');
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Usage: %s <chaine1> <chaine2>\n", argv[0]);
        return 1;
    }


    if (nocase_equal(argv[1], argv[2])) {
        printf("identiques\n");
    } else {
        printf("différentes\n");
    }

    return 0;
}

