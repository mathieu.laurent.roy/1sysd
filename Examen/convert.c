#include <stdio.h>

double convertirPoucesEnCm(double pouces) {
    return pouces * 2.54;
}

double convertirCmEnPouces(double cm) {
    return cm / 2.54;
}

int main() {
    double longueur, resultat;
    int choixConversion;

    printf("Choisissez une conversion :\n");
    printf("1. Convertir de pouces en centimètres\n");
    printf("2. Convertir de centimètres en pouces\n");
    scanf("%d", &choixConversion);

    if (choixConversion == 1) {
        printf("Entrez la longueur en pouces : ");
        scanf("%lf", &longueur);
        resultat = convertirPoucesEnCm(longueur);
        printf("%.2f pouces correspondent à %.2f centimètres.\n", longueur, resultat);
    } else if (choixConversion == 2) {
        printf("Entrez la longueur en centimètres : ");
        scanf("%lf", &longueur);
        resultat = convertirCmEnPouces(longueur);
        printf("%.2f centimètres correspondent à %.2f pouces.\n", longueur, resultat);
    } else {
        printf("Choix non valide. Veuillez choisir 1 ou 2.\n");
    }

    return 0;
}

