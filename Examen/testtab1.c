#include <stdio.h>

void display_tab(int tab[], int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", tab[i]);
    }
    printf("\n");
}

int nb_uniq(int tab[], int size, int newtab[]) {
    int count = 0;  // Compteur pour le nouveau tableau

    for (int i = 0; i < size; i++) {
        if (i == 0 || tab[i] != tab[i - 1]) {
            newtab[count++] = tab[i];
        } else {
            printf("Doublon détecté: %d\n", tab[i]);
        }
    }

    return count;  // Retourner le nombre d'éléments uniques
}

int main() {
    int t1[] = {1, 2, 5, 7, 10}; // pas de doublons
    int t2[] = {1, 1, 4, 5, 42, 42, 42, 4, 5, 42}; // doublons : 1, 1 et 42, 42, 42
    int new_t1[50], new_t2[50];                      // taille maxi 50 pour le résultat.

    printf("Avant nb_uniq pour t1 : ");
    display_tab(t1, 5);

    int new_size_t1 = nb_uniq(t1, 5, new_t1);
    printf("Après nb_uniq pour t1 : ");
    display_tab(new_t1, new_size_t1);

    printf("\nAvant nb_uniq pour t2 : ");
    display_tab(t2, 10);

    int new_size_t2 = nb_uniq(t2, 10, new_t2);
    printf("Après nb_uniq pour t2 : ");
    display_tab(new_t2, new_size_t2);

    return 0;
}

