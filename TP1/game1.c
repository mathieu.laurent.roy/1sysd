#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    srand(time(NULL));  // initialiser la graine pour le générateur de nombres aléatoires

    // Générer un nombre aléatoire entre 1 et 10
    int nombreAleatoire = rand() % 10 + 1;

    int conjecture;

    // Demander à l'utilisateur de deviner le nombre
    printf("Devinez le nombre entre 1 et 10 : ");
    scanf("%d", &conjecture);

    // Vérifier si la conjecture est correcte
    if (conjecture == nombreAleatoire) {
        printf("Félicitations ! Vous avez deviné le bon nombre : %d\n", nombreAleatoire);
    } else {
        printf("Désolé, vous n'avez pas deviné le bon nombre. Le nombre était : %d\n", nombreAleatoire);
    }

    return 0;
}

